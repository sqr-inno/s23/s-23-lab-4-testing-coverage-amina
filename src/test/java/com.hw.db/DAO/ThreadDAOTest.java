import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;

// full statement coverage for function treeSort
public class ThreadDAOTest {
    JdbcTemplate mockJdbc;
    ThreadDAO mockThread;

  @BeforeEach
  void init() {
    mockJdbc = Mockito.mock(JdbcTemplate.class);
    mockThread = new ThreadDAO(mockJdbc);
  }

  private static Stream<Arguments> arguments() {
    return Stream.of(
        Arguments.of(0, 1, 1, true,
            "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
        Arguments.of(0, 1, 1, false,
            "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;"));
  }

  @ParameterizedTest
  @MethodSource("arguments")
  void TestTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String expected) {
    ThreadDAO.treeSort(id, limit, since, desc);
    Mockito.verify(mockJdbc).query(Mockito.eq(expected), Mockito.any(PostDAO.PostMapper.class), Mockito.any());
  }
}
