import java.util.stream.Stream;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Test;

class UserDAOTest {
    JdbcTemplate mockJdbc;
    UserDAO mockUser;

    @BeforeEach
    void init() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        mockUser = new UserDAO(mockJdbc);
    }

    static Stream<Arguments> arguments() {
        return Stream.of(
            Arguments.of("nickname", "email", "fullname", "about", 
            "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE nickname=?::CITEXT;"),
            Arguments.of("nickname", "email", "fullname", null, 
            "UPDATE \"users\" SET  email=? , fullname=?  WHERE nickname=?::CITEXT;"),
            Arguments.of("nickname", "email", null, "about", 
            "UPDATE \"users\" SET  email=? , about=?  WHERE nickname=?::CITEXT;"),
            Arguments.of("nickname", "email", null, null, 
            "UPDATE \"users\" SET  email=?  WHERE nickname=?::CITEXT;"),
            Arguments.of("nickname", null, "fullname", "about", 
            "UPDATE \"users\" SET  fullname=? , about=?  WHERE nickname=?::CITEXT;"),
            Arguments.of("nickname", null, "fullname", null, 
            "UPDATE \"users\" SET  fullname=?  WHERE nickname=?::CITEXT;"),
            Arguments.of("nickname", null, null, "about",
            "UPDATE \"users\" SET  about=?  WHERE nickname=?::CITEXT;")        
        );
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void TestChangeSuccess(String nickname, String email, String fullname, String about, String expected) {
        User user = new User(nickname, email, fullname, about);
        UserDAO.Change(user);
        Mockito.verify(mockJdbc).update(Mockito.eq(expected), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void TestChangeNoUpdate() {
        User user = new User("nickname", null, null, null);
        UserDAO.Change(user);
        Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.any(), Optional.ofNullable(Mockito.any()));
    }
}
