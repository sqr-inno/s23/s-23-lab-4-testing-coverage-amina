import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;


public class ForumDAOTest {
    JdbcTemplate mockJdbc;
    ForumDAO mockForum;

    @BeforeEach
    void init() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        mockForum = new ForumDAO(mockJdbc);
    }

    static Stream<Arguments> arguments() {
        return Stream.of(
                // since, desc, limit
                Arguments.of("slug", 1, "since", true, 
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;"),
                // since, desc, null
                Arguments.of("slug", null, "since", true, 
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;"),
                // since, null, limit
                Arguments.of("slug", 1, "since", null, 
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;"),
                // since, null, null
                Arguments.of("slug", null, "since", null, 
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;"),
                // null, desc, limit
                Arguments.of("slug", 1, null, true, 
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;"),
                // null, desc, null
                Arguments.of("slug", null, null, true, 
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc;"),
                // null, null, limit
                Arguments.of("slug", 1, null, null, 
                "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;"),
                // null, null, null
                Arguments.of("slug", null, null, null, "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;")
        );
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void TestUserList(String slug, Integer limit, String since, Boolean desc, String expected) {
        ForumDAO.UserList(slug, limit, since, desc);
        Mockito.verify(mockJdbc).query(Mockito.eq(expected), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

}
