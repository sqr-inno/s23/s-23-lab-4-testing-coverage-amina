import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;


class PostDAOTest {
    JdbcTemplate mockJdbc;
    PostDAO mockPost;
    Post post;

    static Integer postId = 0;
    static String postQuery = "SELECT * FROM \"posts\" WHERE id=? LIMIT 1;";
    static String author1 = "author1";
    static String author2 = "author2";
    static String message1 = "message1";
    static String message2 = "message2";
    static Timestamp created1 = new Timestamp(0);
    static Timestamp created2 = new Timestamp(1);

    @BeforeEach
    void init() {
        mockJdbc = Mockito.mock(JdbcTemplate.class);
        mockPost = new PostDAO(mockJdbc);
        post = new Post();
        post.setAuthor(author1);
        post.setMessage(message1);
        post.setCreated(created1);

        Mockito.when(
            mockJdbc.queryForObject(
                Mockito.eq(postQuery),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(postId)
            )
        ).thenReturn(post);
    }

    static Stream<Arguments> arguments() {
        return Stream.of(
            Arguments.of(postId, author1, message1, created2, 
            "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(postId, author1, message2, created1, 
            "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
            Arguments.of(postId, author1, message2, created2, 
            "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(postId, author2, message1, created1, 
            "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
            Arguments.of(postId, author2, message1, created2, 
            "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
            Arguments.of(postId, author2, message2, created1, 
            "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
            Arguments.of(postId, author2, message2, created2, 
            "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;")
        );
    }

    @ParameterizedTest
    @MethodSource("arguments")
    void TestSetPostSuccess(Integer postId, String author, String message, Timestamp created, String expected) {
        Post post = new Post();
        post.setAuthor(author);
        post.setMessage(message);
        post.setCreated(created);
        PostDAO.setPost(postId, post);
        Mockito.verify(mockJdbc).update(Mockito.eq(expected), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void TestSetPostNotCalled() {
        PostDAO.setPost(postId, post);
        Mockito.verify(mockJdbc, Mockito.never()).update(Mockito.anyString(), Optional.ofNullable(Mockito.any()));
    }
}
